int sensorPresenca = 12;
int led = 7; // led apenas para representar o status do sensor
int acionamento;

void setup() {
  pinMode(sensorPresenca, INPUT);
  pinMode(led, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  //captura os dados emitidos pelo sensor
  acionamento = digitalRead(sensorPresenca);

  // caso o sensor nao seja acionado e pq nao detectou nenhum movimento
  if (acionamento == LOW) {  
    digitalWrite(led, LOW);
  } else {
    digitalWrite(led, HIGH);
  }
}
